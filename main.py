#!/usr/bin/python3
import pandas as pd
from pyrogram import Client

app = Client("OSMIran")

ban_list = pd.read_csv("fbanned_users.csv")
app.start()

osm_group = app.get_chat("@OpenStreetMap")

# Get member list
member_ids = []
for batch in range(0, 2000, 200):
    for member in osm_group.get_members(offset=batch):
        member_ids.append(member.user.id)

# Bam!
for ban_id in ban_list["id"]:
    if ban_id in member_ids:
        try:
            osm_group.kick_member(ban_id)
            print(f"Member {ban_id} is banned.")
        except:
            print(f"User with {ban_id} could not be found")

app.stop()